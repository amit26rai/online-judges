#include <bits/stdc++.h>
 
using namespace std;
 
int main() {
    int t;
    cin>>t;
    while(t--) {
        int n;
        cin>>n;
        vector<int> v(n);
        for (int i = 0; i < n; i++) {
            cin>>v[i];
        }
        vector<vector<int> > dp(2, vector<int>(200005, INT_MAX));
        int turn = 0;//0,1,2,3
        dp[0][1] = v[0];
        turn++;
        for (int i = 1; i < n; i++) {
            dp[0][i] = min(dp[0][i-1], dp[1][i-1]) + v[i];
            dp[1][i] = min(dp[0][i-1], dp[1][i-1]);
        }      
    }

    return 0;
}